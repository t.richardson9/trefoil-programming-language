open Ast
open Errors

let string_of_dynenv_entry (name, entry) =
  match entry with
  | VarEntry e -> name ^ " -> " ^ string_of_expr e
  

let rec lookup dynenv name =
  match dynenv with
  | [] -> None
  | (x, entry) :: tl ->
    if x = name 
    then Some entry 
    else lookup tl name

let rec interpret_pattern pattern value =      
  match pattern, value with
  | WildcardPattern, _ -> Some []
  | ConsPattern (p1, p2), Cons (v1, v2) -> begin
    match interpret_pattern p1 v1, interpret_pattern p2 v2 with
    | Some l1, Some l2 -> Some (l1 @ l2)
    | _ -> None
  end
        (* TODO: add cases for other kinds of patterns here *)
  | IntPattern n, IntLit m when n = m -> Some []
  | BoolPattern b, BoolLit b' when b = b' -> Some []
  | SymbolPattern s, Symbol s' when s = s' -> Some []
  | NilPattern, Nil -> Some []
  | NilPattern, _ -> None
  | VarPattern var, v -> Some [(var, VarEntry v)]
  | StructPattern (struct_name, sub_patterns), StructConstructor (name, values) ->
    if struct_name <> name || List.length sub_patterns <> List.length values then
      None
    else
      let rec match_sub_patterns sub_pats vals acc =
      match sub_pats, vals with
      | [], [] -> Some acc 
      | pat :: pats, value :: vals -> (
        match interpret_pattern pat value with
        | Some env_extension -> match_sub_patterns pats vals (acc @ env_extension)
        | None -> None  
      )
      | _ -> None  
      in
    match_sub_patterns sub_patterns values [] 
    | _ -> None

let rec interpret_expression dynenv e =
  match e with
  | IntLit _ -> e
  | BoolLit _ -> e
  | Nil -> e
  | Cons (e1, e2) ->
    let val1 = interpret_expression dynenv e1 in
    let val2 = interpret_expression dynenv e2 in
    Cons (val1, val2)
  | Variable x -> begin
    match lookup dynenv x with
    | Some (VarEntry v) -> v
    | None -> raise (RuntimeError ("Unbound variable: " ^ x))
  end
  | Plus (e1, e2) -> begin
    match (interpret_expression dynenv e1, interpret_expression dynenv e2) with
    | (IntLit n1, IntLit n2) -> IntLit (n1 + n2)
    | _, _ -> raise (RuntimeError "Both arguments must be integers")
  end
  | Minus (e1, e2) -> begin
    match interpret_expression dynenv e1, interpret_expression dynenv e2 with
    | IntLit n1, IntLit n2 -> IntLit (n1 - n2)
    | IntLit _, v2 -> raise (RuntimeError ("Operator - applied to non-integer " ^ string_of_expr v2))
    | v1, _ -> raise (RuntimeError ("Operator - applied to non-integer " ^ string_of_expr v1))
  end
  | Multiply (e1, e2) -> begin
    match interpret_expression dynenv e1, interpret_expression dynenv e2 with
    | IntLit n1, IntLit n2 -> IntLit (n1 * n2)
    | IntLit _, v2 -> raise (RuntimeError ("Operator * applied to non-integer " ^ string_of_expr v2))
    | v1, _ -> raise (RuntimeError ("Operator * applied to non-integer " ^ string_of_expr v1))
  end
  | Equal (e1, e2) -> begin
    match interpret_expression dynenv e1, interpret_expression dynenv e2 with
    | IntLit n1, IntLit n2 -> BoolLit (n1 = n2)
    | IntLit _, v2 -> raise (RuntimeError ("Operator = applied to non-integer " ^ string_of_expr v2))
    | v1, _ -> raise (RuntimeError ("Operator = applied to non-integer " ^ string_of_expr v1))
  end
  | If (cond, e1, e2) -> begin
    match interpret_expression dynenv cond with
    | BoolLit true -> interpret_expression dynenv e1
    | BoolLit false -> interpret_expression dynenv e2
    | _ -> interpret_expression dynenv e1
  end
  | IsNil e1 -> begin
    match interpret_expression dynenv e1 with
    | Nil -> BoolLit true
    | _ -> BoolLit false
  end
  | IsCons e1 -> begin
    match interpret_expression dynenv e1 with
    | Cons (_, _) -> BoolLit true
    | _ -> BoolLit false
  end
  | Car e1 -> begin
    match interpret_expression dynenv e1 with
    | Cons (head, _) -> head
    | _ -> raise (RuntimeError "Car applied to non-cons value")
  end
  | Cdr e1 -> begin
    match interpret_expression dynenv e1 with
    | Cons (_, tail) -> tail
    | _ -> raise (RuntimeError "Cdr applied to non-cons value")
  end
  | Let (bindings, body) -> begin
    let evaluated_bindings = List.map (fun (var, defn) -> 
      (var, VarEntry (interpret_expression dynenv defn))) bindings in
    let new_env = evaluated_bindings @ dynenv in
    interpret_expression new_env body
  end
  | Cond clauses ->
    let rec eval_clause = function
      | [] -> raise (RuntimeError "Cond evaluated to false")
      | (cond, expr) :: rest ->
      match interpret_expression dynenv cond with
      | BoolLit true -> interpret_expression dynenv expr
      | BoolLit false -> eval_clause rest
      | _ -> raise (RuntimeError "Cond condition is not boolean")
    in
    eval_clause clauses
  | FunctionCall (func_name, args) -> begin
    match lookup dynenv func_name with
    | Some (VarEntry Closure (param_names, body, defining_env)) ->        
      if List.length args <> List.length param_names then
        raise (RuntimeError "Incorrect number of arguments in function call")
      else
        let arg_values = List.map (fun arg -> interpret_expression dynenv arg) args in
        let arg_env_entries = List.map2 (fun name value -> (name, VarEntry value)) param_names arg_values in
        let combined_env = defining_env @ (List.filter (fun (name, _) -> not (List.mem_assoc name arg_env_entries)) dynenv) in
        let new_env = arg_env_entries @ combined_env in
        interpret_expression new_env body
     | _ -> 
       raise (RuntimeError ("Undefined function: " ^ func_name))
  end
  | Symbol s -> Symbol s
  | Lambda (param_names, body) -> 
    Closure (param_names, body, dynenv)
  | LambdaFunction (expr, arg_exprs) -> begin
      match expr with
      | Lambda (param_names, lambda_body) ->
        let evaluated_args = List.map (fun arg -> interpret_expression dynenv arg) arg_exprs in
        if List.length evaluated_args <> List.length param_names then
          raise (RuntimeError "Incorrect number of arguments for lambda")
        else
          let arg_env_entries = List.map2 (fun name value -> (name, VarEntry value)) param_names evaluated_args in
          let new_env = arg_env_entries @ dynenv in
          interpret_expression new_env lambda_body
      | _ -> 
        raise (RuntimeError "LambdaApplication expects a Lambda as the first argument")
  end
  | Print expr -> begin
    let value = interpret_expression dynenv expr in
    let value_str = string_of_expr value in
    print_endline value_str;
    value
  end
  | StructConstructor (struct_name, args) -> begin
    let arg_values = List.map (fun arg -> interpret_expression dynenv arg) args in
    StructConstructor (struct_name, arg_values)
  end
  | StructPredicate (struct_name, expr) -> begin
    let value = interpret_expression dynenv expr in
    match value with
    | StructConstructor (name, _) when name = struct_name -> BoolLit true
    | _ -> BoolLit false
  end
  | Closure(param_names, body, defining_env) -> Closure(param_names, body, defining_env)
  | StructAccess (struct_name, index, expr) -> begin
    let value = interpret_expression dynenv expr in
    match value with
    | StructConstructor (name, args) ->
      if name = struct_name then
        if index >= 0 && index < List.length args then
          List.nth args index
        else
          raise (RuntimeError "StructAccess: Index out of bounds")
      else
        raise (RuntimeError "StructAccess: Struct name does not match")
  | _ -> raise (RuntimeError "StructAccess: Not a struct constructor")
  end
  | Match (expr, clauses) -> begin
    let value = interpret_expression dynenv expr in
    let rec evaluate_clauses = function
      | [] -> raise (AbstractSyntaxError "No match clause matched")
      | (pattern, expr) :: rest -> begin
       match interpret_pattern pattern value with
      | Some env_extension ->
        let extended_env = env_extension @ dynenv in
        interpret_expression extended_env expr
      | None -> evaluate_clauses rest
      end
    in
    evaluate_clauses clauses
end
    
 
let interpret_binding dynenv b =
  match b with
  | VarBinding (x, e) ->
    let v = interpret_expression dynenv e in
    (x, VarEntry v) :: dynenv

  | FunctionBinding (func_name, params, body) ->
    let closure = Closure (params, body, dynenv) in
    let new_env = (func_name, VarEntry closure) :: dynenv in
    new_env

  | TopLevelExpr e ->
    let v = interpret_expression dynenv e in
    print_endline (string_of_expr v);
    dynenv

  | TestBinding e ->
    begin
      match interpret_expression dynenv e with
      | BoolLit true -> dynenv 
      | BoolLit false -> raise (RuntimeError "Test failed: expression evaluated to false")
      | _ -> raise (RuntimeError "Test failed: non-boolean expression")
    end

  | StructBinding (struct_name, field_names) ->
    let constructor_func = Closure (field_names, StructConstructor (struct_name, List.map (fun field -> Variable field) field_names), dynenv) in
    let new_dynenv = (struct_name, VarEntry constructor_func) :: dynenv in
    let predicate_func = Closure (["x"], StructPredicate (struct_name, Variable "x"), dynenv) in
    let new_dynenv = (struct_name ^ "?", VarEntry predicate_func) :: new_dynenv in
    let accessor_funcs = List.mapi (fun index field_name ->
      let accessor_body = StructAccess (struct_name, index, Variable "struct_instance") in
      let accessor_func = Closure (["struct_instance"], accessor_body, dynenv) in
      (struct_name ^ "-" ^ field_name, VarEntry accessor_func)
      ) field_names in
    List.fold_left (fun env (name, func) -> (name, func) :: env) new_dynenv accessor_funcs


(* the semantics of a whole program (sequence of bindings) *)
let interpret_bindings dynenv bs =
  List.fold_left interpret_binding dynenv bs

(* starting from dynenv, first interpret the list of bindings in order. then, in
   the resulting dynamic environment, interpret the expression and return its
   value *)
let interpret_expression_after_bindings dynenv bindings expr =
  interpret_expression (interpret_bindings dynenv bindings) expr