open Ast
open Errors

type env_entry =
  | VarEntry of expr
  | FunctionEntry of string list * expr * dynamic_env

and dynamic_env = (string * env_entry) list
let string_of_dynenv_entry (name, entry) =
  match entry with
  | VarEntry e -> name ^ " -> " ^ string_of_expr e
  | FunctionEntry (_, _, _) -> name ^ " -> [function]"
let rec lookup dynenv name =
  match dynenv with
  | [] -> None
  | (x, entry) :: tl ->
    if x = name 
    then Some entry 
    else lookup tl name
let rec interpret_expression dynenv e =
  match e with
  | IntLit _ -> e
  | BoolLit _ -> e
  | Nil -> e
  | Cons (e1, e2) ->
    let val1 = interpret_expression dynenv e1 in
    let val2 = interpret_expression dynenv e2 in
    Cons (val1, val2)
  | Variable x -> begin
    match lookup dynenv x with
    | Some (VarEntry v) -> v
    | Some (FunctionEntry _) -> failwith (x ^ " is a function, not a variable")
    | None -> raise (RuntimeError ("Unbound variable: " ^ x))
  end
  | Plus (e1, e2) -> begin
    match (interpret_expression dynenv e1, interpret_expression dynenv e2) with
    | (IntLit n1, IntLit n2) -> IntLit (n1 + n2)
    | _, _ -> raise (RuntimeError "Both arguments must be integers")
    end
    | Minus (e1, e2) -> begin
      match interpret_expression dynenv e1, interpret_expression dynenv e2 with
      | IntLit n1, IntLit n2 -> IntLit (n1 - n2)
      | IntLit _, v2 -> raise (RuntimeError ("Operator - applied to non-integer " ^ string_of_expr v2))
      | v1, _ -> raise (RuntimeError ("Operator - applied to non-integer " ^ string_of_expr v1))
    end
  | Multiply (e1, e2) -> begin
      match interpret_expression dynenv e1, interpret_expression dynenv e2 with
      | IntLit n1, IntLit n2 -> IntLit (n1 * n2)
      | IntLit _, v2 -> raise (RuntimeError ("Operator * applied to non-integer " ^ string_of_expr v2))
      | v1, _ -> raise (RuntimeError ("Operator * applied to non-integer " ^ string_of_expr v1))
    end
  | Equal (e1, e2) -> begin
      match interpret_expression dynenv e1, interpret_expression dynenv e2 with
      | IntLit n1, IntLit n2 -> BoolLit (n1 = n2)
      | IntLit _, v2 -> raise (RuntimeError ("Operator = applied to non-integer " ^ string_of_expr v2))
      | v1, _ -> raise (RuntimeError ("Operator = applied to non-integer " ^ string_of_expr v1))
  
    end
  | If (cond, e1, e2) -> begin
      match interpret_expression dynenv cond with
      | BoolLit true -> interpret_expression dynenv e1
      | BoolLit false -> interpret_expression dynenv e2
      | _ -> interpret_expression dynenv e1
    end
  | IsNil e1 -> begin
      match interpret_expression dynenv e1 with
      | Nil -> BoolLit true
      | _ -> BoolLit false
    end
  | IsCons e1 -> begin
      match interpret_expression dynenv e1 with
      | Cons (_, _) -> BoolLit true
      | _ -> BoolLit false
    end
  | Car e1 -> begin
      match interpret_expression dynenv e1 with
      | Cons (head, _) -> head
      | _ -> raise (RuntimeError "Car applied to non-cons value")
    end
  | Cdr e1 -> begin
      match interpret_expression dynenv e1 with
      | Cons (_, tail) -> tail
      | _ -> raise (RuntimeError "Cdr applied to non-cons value")
    end
  | Let (bindings, body) -> begin
    let evaluated_bindings = List.map (fun (var, defn) -> 
      (var, VarEntry (interpret_expression dynenv defn))) bindings in
    let new_env = evaluated_bindings @ dynenv in
    interpret_expression new_env body
    end
  | Cond clauses ->
      let rec eval_clause = function
        | [] -> raise (RuntimeError "Cond evaluated to false")
        | (cond, expr) :: rest ->
            match interpret_expression dynenv cond with
            | BoolLit true -> interpret_expression dynenv expr
            | BoolLit false -> eval_clause rest
            | _ -> raise (RuntimeError "Cond condition is not boolean")
      in
      eval_clause clauses
      | FunctionCall (func_name, args) -> begin
        match lookup dynenv func_name with
        | Some (FunctionEntry (param_names, body, defining_env)) ->
            if List.length args <> List.length param_names then
              raise (RuntimeError "Incorrect number of arguments in function call")
            else
              let arg_values = List.map (fun arg -> interpret_expression dynenv arg) args in
              let arg_env_entries = List.map2 (fun name value -> (name, VarEntry value)) param_names arg_values in
              let combined_env = defining_env @ (List.filter (fun (name, _) -> not (List.mem_assoc name arg_env_entries)) dynenv) in
              let new_env = arg_env_entries @ combined_env in
              interpret_expression new_env body
        | _ -> raise (RuntimeError ("Undefined function: " ^ func_name))
      end


let interpret_binding dynenv b =
  match b with
  | VarBinding (x, e) ->
    let v = interpret_expression dynenv e in
    (x, VarEntry v) :: dynenv
  | FunctionBinding (func_name, params, body) ->
      let func_entry = FunctionEntry (params, body, dynenv) in
    (func_name, func_entry) :: dynenv
  | TopLevelExpr e ->
     let v = interpret_expression dynenv e in
     print_endline (string_of_expr v);
     dynenv
     | TestBinding e -> 
      begin
        match interpret_expression dynenv e with
        | BoolLit true -> dynenv 
        | BoolLit false -> raise (RuntimeError "Test failed: expression evaluated to false")
        | _ -> raise (RuntimeError "Test failed: non-boolean expression")
      end


(* the semantics of a whole program (sequence of bindings) *)
let interpret_bindings dynenv bs =
  List.fold_left interpret_binding dynenv bs

(* starting from dynenv, first interpret the list of bindings in order. then, in
   the resulting dynamic environment, interpret the expression and return its
   value *)
let interpret_expression_after_bindings dynenv bindings expr =
  interpret_expression (interpret_bindings dynenv bindings) expr
