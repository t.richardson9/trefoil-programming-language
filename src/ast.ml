open Errors

type pattern =
  | WildcardPattern
  | ConsPattern of pattern * pattern
(* TODO: add more patterns here *)
[@@deriving show]

let string_of_pattern = show_pattern
type expr =
  | IntLit of int
  | BoolLit of bool
  | Variable of string
  | Plus of expr * expr
  | If of expr * expr * expr
  | Nil
  | Cons of expr * expr
  | IsNil of expr
  | IsCons of expr
  | Car of expr
  | Cdr of expr
  | Minus of expr * expr
  | Multiply of expr * expr
  | Equal of expr * expr
  | Let of (string * expr) list * expr
  | Cond of (expr * expr) list
  | FunctionCall of string * expr list
[@@deriving show]
let string_of_expr = show_expr

(* last stage of parser: converts pst to expr *)
let rec expr_of_pst p =
  match p with
  | Pst.Symbol sym -> begin
     try
       IntLit (int_of_string sym)
     with
       Failure _ ->
       match sym with
       | "true" -> BoolLit true
       | "false" -> BoolLit false
       | "nil" -> Nil
       | _ -> Variable sym
    end
    | Pst.Node (Pst.Symbol "let" :: Pst.Node bindings_pst :: [body_pst]) ->
      let bindings = List.map (function
        | Pst.Node [Pst.Symbol var; value_pst] -> (var, expr_of_pst value_pst)
        | _ -> raise (AbstractSyntaxError "Malformed let binding")
      ) bindings_pst in
      Let (bindings, expr_of_pst body_pst)
  | Pst.Node (Pst.Symbol "cond" :: clauses) ->
        let parse_clause pst = match pst with
          | Pst.Node [cond; expr] -> (expr_of_pst cond, expr_of_pst expr)
          | _ -> raise (AbstractSyntaxError "Malformed cond clause")
        in
        Cond (List.map parse_clause clauses)
  | Pst.Node (Pst.Symbol "cons" :: args) ->
    (match args with
    | [expr1_pst; expr2_pst] -> Cons (expr_of_pst expr1_pst, expr_of_pst expr2_pst)
    | _ -> raise (AbstractSyntaxError "cons expects exactly 2 arguments"))
  | Pst.Node [] -> raise (AbstractSyntaxError "Expected expression but got '()'")
  | Pst.Node (head :: args) ->
      match head, args with
      | Pst.Node _, _ -> raise (AbstractSyntaxError ("Expression forms must start with a symbol, but got " ^ Pst.string_of_pst head))
      | Pst.Symbol "+", [left; right] -> Plus (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "+", _ -> raise (AbstractSyntaxError ("operator + expects 2 args but got " ^ Pst.string_of_pst p))
      | Pst.Symbol "if", [branch; thn; els] -> If (expr_of_pst branch, expr_of_pst thn, expr_of_pst els)
      | Pst.Symbol "if", _ -> raise (AbstractSyntaxError ("'if' special form expects 3 args but got " ^ Pst.string_of_pst p))
      | Pst.Symbol "-", [left; right] -> Minus (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "-", _-> raise (AbstractSyntaxError ("operator - expects 2 args but got " ^ Pst.string_of_pst head))
      | Pst.Symbol "*", [left; right] -> Multiply (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "*", _ -> raise (AbstractSyntaxError ("operator * expects 2 args but got " ^ Pst.string_of_pst head))
      | Pst.Symbol "=", [left; right] -> Equal (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "=", _ -> raise (AbstractSyntaxError ("operator = expects 2 args but got " ^ Pst.string_of_pst head))
      | Pst.Symbol "nil?", [arg] -> IsNil (expr_of_pst arg)
      | Pst.Symbol "nil?", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol "cons?", [arg] -> IsCons (expr_of_pst arg)
      | Pst.Symbol "cons?", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol "car", [arg] -> Car (expr_of_pst arg)
      | Pst.Symbol "car", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol "cdr", [arg] -> Cdr (expr_of_pst arg)
      | Pst.Symbol "cdr", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol f,_ -> FunctionCall (f, List.map expr_of_pst args)

let expr_of_string s =
  s
  |> Pstparser.pst_of_string
  |> expr_of_pst

type binding =
  | VarBinding of string * expr
  | TopLevelExpr of expr
  | TestBinding of expr
  | FunctionBinding of string * string list * expr
[@@deriving show]
let string_of_binding = show_binding

let binding_of_pst p =
  match p with
  | Pst.Symbol _ -> TopLevelExpr (expr_of_pst p)
  | Pst.Node [] -> raise (AbstractSyntaxError "Expected binding but got '()'")
  | Pst.Node (Pst.Symbol "define" :: Pst.Node (Pst.Symbol func_name :: params) :: body :: []) ->
    let param_names = List.map (function
                                | Pst.Symbol s -> s
                                | _ -> raise (AbstractSyntaxError "Parameter names must be symbols")
                              ) params in
    FunctionBinding (func_name, param_names, expr_of_pst body)
  | Pst.Node (head :: args) ->
    match head, args with
    | Pst.Symbol "define", [Pst.Symbol lhs_var; rhs] -> VarBinding (lhs_var, expr_of_pst rhs)
    | Pst.Symbol "define", _ -> raise (AbstractSyntaxError("This definition is malformed " ^ Pst.string_of_pst p))
    | Pst.Symbol "test", [expr] -> TestBinding (expr_of_pst expr)
    | Pst.Symbol "test", _ -> raise (AbstractSyntaxError("A 'test' binding expects a single argument but got " ^ Pst.string_of_pst p))
        
    | Pst.Node _, _ -> raise (AbstractSyntaxError("Expected binding to start with a symbol but got " ^ Pst.string_of_pst p))
    | _ -> raise (AbstractSyntaxError ("Unexpected form: " ^ Pst.string_of_pst p))

let binding_of_string s =
  s
  |> Pstparser.pst_of_string
  |> binding_of_pst

let bindings_of_string s =
  let p = Pstparser.pstparser_of_string s in
  let rec parse_binding_list () =
    match Pstparser.parse_pst p with
    | None -> []
    | Some pst ->
       binding_of_pst pst :: parse_binding_list ()
  in
  parse_binding_list ()
  let pattern_of_pst p =
  match p with
  | Pst.Symbol sym -> begin
      match int_of_string_opt sym with
      | Some n -> failwith "TODO: build an int pattern with n here"
      | None ->
         match sym with
         | "_" -> WildcardPattern
         | "true" -> failwith "TODO: build a bool pattern with true here"
         (* TODO: add other cases here for "false" and "nil" *)
         | _ ->
            if String.get sym 0 = '\'' (* if the string starts with an apostrophe *)
            then let sym_without_apostrophe = String.sub sym 1 (String.length sym - 1)
                 in failwith "TODO: build a symbol pattern using sym_without_apostrophe"
            else failwith "TODO: build a variable pattern using sym"
    end
  | Pst.Node [] -> raise (AbstractSyntaxError "Expected pattern but got '()'")
  | Pst.Node (head :: args) ->
     match head, args with
     | Pst.Symbol "cons", [p1; p2] -> ConsPattern (pattern_of_pst p1, pattern_of_pst p2)
     | Pst.Symbol s, ps -> failwith "TODO: build a struct pattern using patterns ps"
     | _ -> raise (AbstractSyntaxError ("Expected pattern, but got " ^ Pst.string_of_pst p))

let pattern_of_string s =
  s
  |> Pstparser.pst_of_string
  |> pattern_of_pst
