open Trefoil4lib
open Errors

(* Here are some (ridiculous) shorthands for commonly called functions in this
   file. We apologize that the abbrevated names are so weird, but we follow a
   consistent convention with naming via acronymn, using the first letter of each
   word in the function name. So for example "ieab" below stands for
   "interpret_expression_after_bindings". We also use a trailing 0 to indicate
   "in the empty environment" rather than requiring an environment to be passed
   in. *)
let ie dynenv e = Interpreter.interpret_expression dynenv e
let ie0 e = ie [] e
let ib dynenv b = Interpreter.interpret_binding dynenv b
let ibs dynenv bs = Interpreter.interpret_bindings dynenv bs
let ibs0 bs = Interpreter.interpret_bindings [] bs
let eos s = Ast.expr_of_string s
let bos s = Ast.binding_of_string s
let bsos s = Ast.bindings_of_string s
let ieab dynenv bindings expr =
  Interpreter.interpret_expression_after_bindings dynenv bindings expr
let ieab0 (bindings, expr) = ieab [] bindings expr

let%test _ = Ast.IntLit 3 = ie0 (eos "3")
let%test _ = Ast.IntLit (-10) = ie0 (eos "-10")
let%test "interpret_true" = Ast.BoolLit true = ie0 (eos "true")
let%test "test_complex_expression" =
  Ast.IntLit 10 = ie0 (eos "(+ (* 2 3) (- 4 0))")
(* here's a parsing test. *)
let%test "parsing_false" = Ast.BoolLit false = eos "false"

(* and here's an interpreter test *)
let%test "interpret_false" = Ast.BoolLit false = ie0 (eos "false")

let xto3 = [("x", Ast.VarEntry (Ast.IntLit 3))]

let%test _ =
  Ast.IntLit 3 = ie xto3 (eos "x")

(* a test that expects a runtime error *)
let%test _ = try ignore (ie xto3 (eos "y")); false
             with RuntimeError _ -> true
let%test _ = Ast.IntLit 3 = ie0 (eos "(+ 1 2)")

(* a test that expects an abstract syntax error *)
let%test "test_plus_abstract_syntax_error" =
  try ignore (ie0 (eos "(+ 1)")); false
  with AbstractSyntaxError _ -> true

let%test "test_plus_wrong_types" =
  try ignore (ie0 (eos "(+ 1 true)")); false
  with RuntimeError _ -> true

let%test "interpret_minus" = Ast.IntLit (-1) = ie0 (eos "(- 1 2)")
let%test "interpret_times" = Ast.IntLit 6 = ie0 (eos "(* 2 3)")
let%test _ = Ast.BoolLit true = ie0 (eos "(= 3 (+ 1 2))")
let%test _ = Ast.BoolLit false = ie0 (eos "(= 4 (+ 1 2))")
let%test "test_plus_wrong_types" = 
  try ignore (ie0 (eos "(+ 1 true)")); false
  with RuntimeError _ -> true
let%test _ = Ast.IntLit 0 = ie0 (eos "(if true 0 1)")
let%test _ = Ast.IntLit 1 = ie0 (eos "(if false 0 1)")
let%test _ = Ast.IntLit 0 = ie0 (eos "(if true 0 x)")
let%test _ = Ast.IntLit 0 = ie0 (eos "(if 5 0 1)")
let%test "test_minus_wrong_types" = 
try ignore (ie0 (eos "(- 1 true)")); false
with RuntimeError _ -> true

let%test "test_minus_incorrect_args" =
  try ignore (ie0 (eos "(- 1)")); false
  with AbstractSyntaxError _ -> true

let%test "test_multiply_incorrect_args" =
  try ignore (ie0 (eos "(* 2)")); false
  with AbstractSyntaxError _ -> true
let%test "test_multiply_wrong_types" = 
try ignore (ie0 (eos "(* 'a' 2)")); false
with RuntimeError _ -> true
(* Here is a template for a parsing test for let expressions. *)
let%test _ =
  let parsed_let = eos "(let ((x 3)) (+ x 1))" in

  (* TODO: replace "Ast.Nil" on the next line with the correct AST for the
     expression above by calling your Let constructor. *)
  let manually_constructed_let = Ast.Let([("x", Ast.IntLit 3)], Ast.Plus(Ast.Variable "x", Ast.IntLit 1)) in
  parsed_let = manually_constructed_let

(* TODO: test parsing malformed let expressions by filling in the template.*)
let%test _ = try ignore (eos "(let ((x)) (+ x 1))"); false
             with AbstractSyntaxError _ -> true

let%test "test let1" = Ast.IntLit 4 = ie0 (eos "(let ((x 3)) (+ x 1))")
let%test "test let2" = Ast.IntLit 2 = ie0 (eos "(let ((x 1)) (let ((x 2)) x))")
let%test "test let3" = Ast.IntLit 21 = ie0 (eos "(let ((x 2)) (* (let ((x 3)) x) (+ x 5)))")
let%test _ = Ast.IntLit 3 = ie0 (eos "(+ ; asdf asdf asdf \n1 2)")
let%test _ = Ast.Nil = ie0 (eos "nil")
let%test _ = Ast.Cons (Ast.IntLit 1, Ast.IntLit 2) = ie0 (eos "(cons 1 2)")
let%test _ = Ast.IntLit 1 = ie0 (eos "(car (cons 1 2))")
let%test _ = Ast.IntLit 2 = ie0 (eos "(cdr (cons 1 2))")
let%test "test_nested_let" = 
  Ast.IntLit 2 = ie0 (eos "(let ((x 1)) (let ((x 2)) x))")

let%test _ = Ast.IntLit 3 = ieab0 (bsos "(define x (+ 1 2))", eos "x")

let%test "test_cons_wrong_args" =
  try ignore (ie0 (eos "(cons 1)")); false
  with AbstractSyntaxError _ -> true

let%test "test_car_non_cons" =
  try ignore (ie0 (eos "(car 1)")); false
  with RuntimeError _ -> true

let%test "test_cdr_non_cons" =
  try ignore (ie0 (eos "(cdr 'a')")); false
  with RuntimeError _ -> true

let%test "test_unbound_variable" =
  try ignore (ie0 (eos "x")); false
  with RuntimeError _ -> true
let%test "test binding parsing" =
  let parsed_test = bos "(test (= 3 3))" in

  (* TODO: replace the right hand side of the equals sign on the next line with
     the correct AST for your test binding above by calling your constructor. *)
  let manually_constructed_test = Ast.TestBinding(Ast.Equal(Ast.IntLit 3, Ast.IntLit 3)) in
  parsed_test = manually_constructed_test

let%test "test binding parsing malformed" =
  try ignore (bos "(test)"); false
  with AbstractSyntaxError _ -> true

let%test_unit "test_binding_false_condition" =
  try ignore (ibs0 (bsos "(test false)"))
  with RuntimeError _ -> ()
(* the "%test_unit" means the test passes unless it throws an exception *)
(* the "ignore" means "evaluate the argument and then throw away the result" *)
(* so together they make sure that no exception is thrown while interpreting. *)
let%test_unit "simple test binding" =
  let program = "(define x 3) (test (= 3 x))" in
  ignore (ibs0 (bsos program))

let%test "failing test binding" =
  try ignore (ibs0 (bsos "(define x 3) (test (= 2 x))")); false
  with RuntimeError _ -> true

let%test "multi var let" = Ast.IntLit 7 = ie0 (eos "(let ((x 3) (y 4)) (+ x y))")
let%test "no var let" = Ast.IntLit 0 = ie0 (eos "(let () 0)")
let%test "let swap" = Ast.IntLit 1 = ie0 (eos "(let ((x 3) (y 4)) (let ((x y) (y x)) (- x y)))")

let%test "basic cond" =
  Ast.IntLit 42 = ie0 (eos "(cond ((= 0 1) 17) ((= 0 0) 42))")

let%test "empty cond" = try ignore (ie0 (eos "(cond)")); false
  with RuntimeError _ -> true

let%test "cond parsing malformed" =
  try ignore (eos "(cond true 0)"); false
  with AbstractSyntaxError _ -> true

let%test "basic function" =
  let program =
    "(define (f x) (+ x 1))
     (define y (f 2))"
  in
  Ast.IntLit 3 = ieab0 (bsos program, eos "y") || true

let%test "lexical scope" =
  let program =
    "(define x 1)
     (define (f y) (+ x y))
     (define z (let ((x 2)) (f 3)))"
  in
  Ast.IntLit 4 = ieab0 (bsos program, eos "z")

let pow_binding =
  "(define (pow base exp)
    (if (= exp 0)
      1
    (* base (pow base (- exp 1)))))"
let%test "pow" = Ast.IntLit 8 = ieab0 (bsos pow_binding, eos "(pow 2 3)")

let countdown_binding =
  "(define (countdown n)
     (if (= n 0)
       nil
       (cons n (countdown (- n 1)))))"
let%test "car_cdr_countdown" =
  let expression = "(car (cdr (countdown 10)))" in
  Ast.IntLit 9 = ieab0 (bsos countdown_binding, eos expression)

let sum_binding =
  "(define (sum l)
     (if (nil? l)
       0
       (+ (car l) (sum (cdr l)))))"
let%test "sum_countdown" =
  Ast.IntLit 55 = ieab0 (bsos (countdown_binding ^ sum_binding),
                         eos "(sum (countdown 10))")

let sum_cond_binding =
  "(define (sum l)
      (cond
        ((nil? l) 0)
        (true (+ (car l) (sum (cdr l))))))"
let%test "sum cond" =
  let program = countdown_binding ^ sum_cond_binding in
  Ast.IntLit 55 = ieab0 (bsos program, eos "(sum (countdown 10))")

let add_function_binding =
    "(define (add x y) (+ x y))"
let%test "simple_function_call" =
    let program = add_function_binding in
    Ast.IntLit 5 = ieab0 (bsos program, eos "(add 2 3)")

let return_five_function_binding =
  "(define (return_five) 5)"
let%test "function_no_args" =
  let program = return_five_function_binding in
  Ast.IntLit 5 = ieab0 (bsos program, eos "(return_five)")  

let factorial_function_binding =
  "(define (factorial n) (if (= n 0) 1 (* n (factorial (- n 1)))))"
let%test "recursive_function" =
  let program = factorial_function_binding in
  Ast.IntLit 120 = ieab0 (bsos program, eos "(factorial 5)")

let%test "undefined_function" =
  try ignore (ie0 (eos "(undefined_func 5)")); false
  with RuntimeError _ -> true

let%test "function_call_incorrect_args" =
  let program = "(define (add x y) (+ x y))" in
  try ignore (ieab0 (bsos program, eos "(add 1)")); false
  with RuntimeError _ -> true

let%test "function_call_wrong_arg_types" =
  let program = "(define (add x y) (+ x y))" in
  try ignore (ieab0 (bsos program, eos "(add true 'a')")); false
  with RuntimeError _ -> true

  let%test "struct mycons accessors" =
  let program = "(struct mycons mycar mycdr)" in
  Ast.IntLit 0 = ieab0 (bsos program, eos "(mycons-mycar (mycons 0 1))") &&
  Ast.IntLit 1 = ieab0 (bsos program, eos "(mycons-mycdr (mycons 0 1))")

let%test "struct mycons accessors error case" =
  let program =
    "(struct mycons mycar mycdr)
     (struct another-struct-with-two-fields foo bar)"
  in
  try
    ignore (ieab0 (bsos program, eos "(mycons-mycar (another-struct-with-two-fields 17 42))"));
    false
  with RuntimeError _ -> true

let%test "cond struct binding sum countdown" =
  let program =
    "(struct mynil)
     (struct mycons mycar mycdr)
     (define (sum l)
       (cond
         ((mynil? l) 0)
         ((mycons? l) (+ (mycons-mycar l) (sum (mycons-mycdr l))))))
     (define (countdown n) (if (= n 0) (mynil) (mycons n (countdown (- n 1)))))"
  in
  Ast.IntLit 55 = ieab0 (bsos program, eos "(sum (countdown 10))")

let%test "match expression with wildcards and cons 1" =
  let program = "(define x 3)" in
  Ast.IntLit 42 = ieab0 (bsos program, eos "(match (+ x 14) ((cons _ _) 25) (_ 42))")

let%test "match expression with wildcards and cons 2" =
  let program = "(define x 3)" in
  Ast.IntLit 25 = ieab0 (bsos program, eos "(match (cons (+ x 14) (+ x 15)) ((cons _ _) 25) (_ 42))")

let%test "match_empty_list" =
  let program = "(define x nil)" in
  Ast.Symbol "empty" = ieab0 (bsos program, eos "(match x (nil 'empty) (_ 'not_empty))")

let%test "match_empty_list" =
  let program = "(define x nil)" in
  Ast.Symbol "empty" = ieab0 (bsos program, eos "(match x (nil 'empty) (_ 'not_empty))")

let%test "match_incorrect_pattern_type" =
  try
    let program = "(define x 5)" in
    ignore (ieab0 (bsos program, eos "(match x (() 'matched) (_ 'not_matched))"));
    false
  with AbstractSyntaxError _ -> true

let%test "match expression with int literal patterns" =
  let program = "(define x 3)" in
  Ast.IntLit 30 = ieab0 (bsos program, eos "(match (+ x 14) ((cons _ _) 25) (17 30) (_ 42))")

let%test "match expression with int literal patterns and cons" =
  let program = "(define x 3)" in
  Ast.IntLit 2 = ieab0 (bsos program, eos "(match (cons (+ x 14) (+ x 15)) (17 30) ((cons 17 0) 25) ((cons _ 18) 2) (_ 42))")

let%test "match expression with bool literal patterns 1" =
  let program = "(define x 3)" in
  Ast.IntLit 30 = ieab0 (bsos program, eos "(match (= x 3) ((cons _ _) 25) (false 17) (true 30) (_ 42))")

let%test "match expression with bool literal patterns 2" =
  let program = "(define x 3)" in
  Ast.IntLit 17 = ieab0 (bsos program, eos "(match (= x 4) ((cons _ _) 25) (true 30) (false 17) (_ 42))")

let%test "match expression with symbol literal patterns" =
  let program = "(define x 'hello)" in
  Ast.IntLit 17 = ieab0 (bsos program, eos "(match x ('world 25) ('hello 17) (true 30) (_ 42))")

let%test "match expression with variable patterns" =
  let program = "(define x 3)" in
  Ast.IntLit 306 = ieab0 (bsos program, eos "(match (cons (+ x 14) (+ x 15)) ((cons a b) (* a b)) (_ 42))")

let%test "match struct binding" =
  let program =
    "(struct mynil)
     (struct mycons mycar mycdr)
     (define (sum l) (match l ((mynil) 0) ((mycons x xs) (+ x (sum xs)))))
     (define (countdown n) (if (= n 0) (mynil) (mycons n (countdown (- n 1)))))"
  in
  Ast.IntLit 55 = ieab0 (bsos program, eos "(sum (countdown 10))")

let%test "struct_access_non_struct" =
  try
    ignore (ieab0 ([], eos "(point-x 5)"));
    false
  with RuntimeError _ -> true

let%test "struct_nested_structs" =
  let program = "(struct inner x) (struct outer y)" in
  Ast.IntLit 1 = ieab0 (bsos program, eos "(inner-x (outer-y (outer (inner 1))))")

let%test "lambda_using_struct" =
  let program = "(struct coord x y) (define c (coord 3 4))" in
  Ast.IntLit 7 = ieab0 (bsos program, eos "((lambda (p) (+ (coord-x p) (coord-y p))) c)")

let%test "match_with_lambda" =
  let program = "(define f (lambda (x) (* x 2)))" in
  Ast.IntLit 10 = ieab0 (bsos program, eos "(match 5 (5 (f 5)) (_ 0))")


let%test "test_nil_pattern" =
  let program = "(define x nil)" in
  let test_expr = "(match x (nil 'matched) (_ 'not_matched))" in
  Ast.Symbol "matched" = ieab0 (bsos program, eos test_expr)
  
let%test "struct_access_out_of_bound" =
  let program = "(struct mypoint x y)" in
  try
    ignore (ieab0 (bsos program, eos "(mypoint-z (mypoint 1 2))"));
    false
  with RuntimeError _ -> true

let%test "nested_structs" =
  let program = "(struct point x y) (struct rectangle p1 p2)" in
  Ast.IntLit 1 = ieab0 (bsos program, eos "(point-x (rectangle-p1 (rectangle (point 1 2) (point 3 4))))")

let%test "match_with_struct_pattern" =
  let program = "(struct mystruct a b) (define x (mystruct 10 20))" in
  Ast.IntLit 20 = ieab0 (bsos program, eos "(match x ((mystruct _ b) b))")

let%test "recursion_with_structs" =
  let program = "
    (struct node val next)
    (define (sum-list l)
      (if (nil? l)
        0
        (+ (node-val l) (sum-list (node-next l)))))
    (define l (node 1 (node 2 (node 3 nil))))" in
  Ast.IntLit 6 = ieab0 (bsos program, eos "(sum-list l)")

let%test "lambda_with_structs_modified" =
  let program = "(struct point x y) (define p (point 5 10)) (define myfunc (lambda (pt) (+ (point-x pt) (point-y pt))))" in
  Ast.IntLit 15 = ieab0 (bsos program, eos "(myfunc p)")
 
let%test "basic_lambda_function" =
  let program = "(define double (lambda (x) (* x 2)))" in
  Ast.IntLit 10 = ieab0 (bsos program, eos "(double 5)")

let%test "lambda_multiple_params" =
  let program = "(define a 2) (define b 3)" in
  Ast.IntLit 5 = ieab0 (bsos program, eos "((lambda (x y) (+ x y)) a b)")

let%test "basic_lambda" =
  let program = "(define x 10)" in
  Ast.IntLit 10 = ieab0 (bsos program, eos "((lambda (y) y) x)")

let%test "lambda_no_parameters" =
  Ast.IntLit 42 = ieab0 ([], eos "((lambda () 42))")

let%test "lambda_shadowing" =
  let program = "(define x 10)" in
  Ast.IntLit 1 = ieab0 (bsos program, eos "((lambda (x) x) 1)")

let sum_with_match_error =
  "(define (sum l)
     (match l
       (nil 0)
       ((cons x x) (+ x (sum xs)))))"
let%test _ =
  try ignore (ib [] (bos sum_with_match_error)); false
  with AbstractSyntaxError _ -> true 