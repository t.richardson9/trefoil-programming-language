open Errors

type env_entry =
  | VarEntry of expr
  
  and dynamic_env = (string * env_entry) list
  
  and expr =
  | IntLit of int
  | BoolLit of bool
  | Variable of string
  | Plus of expr * expr
  | If of expr * expr * expr
  | Nil
  | Cons of expr * expr
  | IsNil of expr
  | IsCons of expr
  | Car of expr
  | Cdr of expr
  | Minus of expr * expr
  | Multiply of expr * expr
  | Equal of expr * expr
  | Let of (string * expr) list * expr
  | Cond of (expr * expr) list
  | Symbol of string
  | Print of expr
  | Lambda of string list * expr
  | StructConstructor of string * expr list
  | StructPredicate of string * expr
  | StructAccess of string * int * expr
  | FunctionCall of string * expr list
  | LambdaFunction of expr * expr list
  | Closure of string list * expr * dynamic_env
  | Match of expr * (pattern * expr) list
[@@deriving show]

and pattern =
  | WildcardPattern
  | ConsPattern of pattern * pattern
  | IntPattern of int
  | BoolPattern of bool
  | SymbolPattern of string
  | VarPattern of string
  | NilPattern
  | StructPattern of string * pattern list
[@@deriving show]
let string_of_expr = show_expr
let string_of_pattern = show_pattern

let rec vars_of_pattern pat =
  match pat with
  | WildcardPattern | IntPattern _ | BoolPattern _ | SymbolPattern _ | NilPattern -> []
  | VarPattern var -> [var]
  | ConsPattern (p1, p2) ->
    (vars_of_pattern p1) @ (vars_of_pattern p2)
  | StructPattern (_, ps) ->
    List.flatten (List.map vars_of_pattern ps)

let rec pattern_of_pst p =
  match p with
  | Pst.Symbol sym -> begin
    match int_of_string_opt sym with
    | Some n -> IntPattern n
    | None ->
      match sym with
      | "_" -> WildcardPattern
      | "true" -> BoolPattern true
      | "false" -> BoolPattern false
      | "nil" -> NilPattern
      | _ ->
        if String.get sym 0 = '\'' (* if the string starts with an apostrophe *)
        then let sym_without_apostrophe = String.sub sym 1 (String.length sym - 1)in 
          SymbolPattern sym_without_apostrophe
        else VarPattern sym
  end
  | Pst.Node [] -> raise (AbstractSyntaxError "Expected pattern but got '()'")
  | Pst.Node (head :: args) ->
    match head, args with
    | Pst.Symbol "cons", [p1; p2] -> ConsPattern (pattern_of_pst p1, pattern_of_pst p2)
    | Pst.Symbol s, ps -> 
      let sub_patterns = List.map pattern_of_pst ps in
      StructPattern (s, sub_patterns)
    | _ -> raise (AbstractSyntaxError ("Expected pattern, but got " ^ Pst.string_of_pst p))
let rec expr_of_pst p =
  match p with
  | Pst.Symbol sym -> begin
    try
      IntLit (int_of_string sym)
    with
      Failure _ ->
      match sym with
      | "true" -> BoolLit true
      | "false" -> BoolLit false
      | "nil" -> Nil
      | _ -> 
        if String.length sym > 0 && String.get sym 0 = '\''
        then Symbol (String.sub sym 1 (String.length sym - 1))  
        else 
          try IntLit (int_of_string sym) 
          with Failure _ -> Variable sym
  end
  | Pst.Node (Pst.Symbol "match" :: expr_pst :: clauses_psts) -> begin
    let expr = expr_of_pst expr_pst in
    let clauses = List.map (function
      | Pst.Node [pattern_pst; body_pst] ->
        let pattern = pattern_of_pst pattern_pst in
        let vars = vars_of_pattern pattern in
        if List.length vars <> List.length (List.sort_uniq compare vars) then
          raise (AbstractSyntaxError "Duplicate variables in pattern");
        (pattern, expr_of_pst body_pst)
      | _ -> raise (AbstractSyntaxError "Malformed match clause")
    ) clauses_psts in
    Match (expr, clauses)
    end
  | Pst.Node (Pst.Symbol "print" :: [arg_pst]) -> 
    Print (expr_of_pst arg_pst)

  | Pst.Node (Pst.Symbol "let" :: Pst.Node bindings_pst :: [body_pst]) ->
    let bindings = List.map (function
      | Pst.Node [Pst.Symbol var; value_pst] -> (var, expr_of_pst value_pst)
      | _ -> raise (AbstractSyntaxError "Malformed let binding")
    ) bindings_pst in
    Let (bindings, expr_of_pst body_pst)

  | Pst.Node (Pst.Symbol "lambda" :: Pst.Node params_pst_list :: body_pst :: []) ->
    let params = List.map (function
      | Pst.Symbol param -> param
      | _ -> raise (AbstractSyntaxError "Parameter names must be symbols")
    ) params_pst_list in
    let body = expr_of_pst body_pst in
    Lambda (params, body)

  | Pst.Node (Pst.Symbol "cond" :: clauses) ->
    let parse_clause pst = 
      match pst with
      | Pst.Node [cond; expr] -> (expr_of_pst cond, expr_of_pst expr)
      | _ -> raise (AbstractSyntaxError "Malformed cond clause")
      in
    Cond (List.map parse_clause clauses)

  | Pst.Node (Pst.Symbol "cons" :: args) ->
    (match args with
    | [expr1_pst; expr2_pst] -> Cons (expr_of_pst expr1_pst, expr_of_pst expr2_pst)
    | _ -> raise (AbstractSyntaxError "cons expects exactly 2 arguments"))
  | Pst.Node [] -> raise (AbstractSyntaxError "Expected expression but got '()'")
  | Pst.Node (head :: args) ->  
    match head, args with
      | Pst.Symbol "+", [left; right] -> Plus (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "+", _ -> raise (AbstractSyntaxError ("operator + expects 2 args but got " ^ Pst.string_of_pst p))
      | Pst.Symbol "if", [branch; thn; els] -> If (expr_of_pst branch, expr_of_pst thn, expr_of_pst els)
      | Pst.Symbol "if", _ -> raise (AbstractSyntaxError ("'if' special form expects 3 args but got " ^ Pst.string_of_pst p))
      | Pst.Symbol "-", [left; right] -> Minus (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "-", _-> raise (AbstractSyntaxError ("operator - expects 2 args but got " ^ Pst.string_of_pst head))
      | Pst.Symbol "*", [left; right] -> Multiply (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "*", _ -> raise (AbstractSyntaxError ("operator * expects 2 args but got " ^ Pst.string_of_pst head))
      | Pst.Symbol "=", [left; right] -> Equal (expr_of_pst left, expr_of_pst right)
      | Pst.Symbol "=", _ -> raise (AbstractSyntaxError ("operator = expects 2 args but got " ^ Pst.string_of_pst head))
      | Pst.Symbol "nil?", [arg] -> IsNil (expr_of_pst arg)
      | Pst.Symbol "nil?", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol "cons?", [arg] -> IsCons (expr_of_pst arg)
      | Pst.Symbol "cons?", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol "car", [arg] -> Car (expr_of_pst arg)
      | Pst.Symbol "car", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol "cdr", [arg] -> Cdr (expr_of_pst arg)
      | Pst.Symbol "cdr", _ -> raise (AbstractSyntaxError ("Incorrect number of arguments for operator " ^ Pst.string_of_pst head))
      | Pst.Symbol f,_ ->
        FunctionCall (f, List.map expr_of_pst args)
      |  _, _ ->
        let func_expr = expr_of_pst head in
        let arg_exprs = List.map expr_of_pst args in
        (match func_expr with
        | Variable func_name -> FunctionCall (func_name, arg_exprs)
        | Lambda _-> LambdaFunction (func_expr, arg_exprs)
        | _ -> raise (AbstractSyntaxError "Function call must start with a variable"))

let expr_of_string s =
  s
  |> Pstparser.pst_of_string
  |> expr_of_pst

type binding =
  | VarBinding of string * expr
  | TopLevelExpr of expr
  | TestBinding of expr
  | FunctionBinding of string * string list * expr
  | StructBinding of string * string list
[@@deriving show]
let string_of_binding = show_binding

let binding_of_pst p =
  match p with
  | Pst.Symbol _ -> TopLevelExpr (expr_of_pst p)
  | Pst.Node [] -> raise (AbstractSyntaxError "Expected binding but got '()'")
  | Pst.Node (Pst.Symbol "struct" :: Pst.Symbol struct_name :: field_symbols) ->
    let fields = List.map (function
                            | Pst.Symbol field -> field
                            | _ -> raise (AbstractSyntaxError "Field names must be symbols")
                           ) field_symbols in
    StructBinding (struct_name, fields)
  | Pst.Node (Pst.Symbol "define" :: Pst.Node (Pst.Symbol func_name :: params) :: body :: []) ->
    let param_names = List.map (function
                                | Pst.Symbol s -> s
                                | _ -> raise (AbstractSyntaxError "Parameter names must be symbols")
                              ) params in
    FunctionBinding (func_name, param_names, expr_of_pst body)
  | Pst.Node (head :: args) ->
    match head, args with
    | Pst.Symbol "define", [Pst.Symbol lhs_var; rhs] -> VarBinding (lhs_var, expr_of_pst rhs)
    | Pst.Symbol "define", _ -> raise (AbstractSyntaxError("This definition is malformed " ^ Pst.string_of_pst p))
    | Pst.Symbol "test", [expr] -> TestBinding (expr_of_pst expr)
    | Pst.Symbol "test", _ -> raise (AbstractSyntaxError("A 'test' binding expects a single argument but got " ^ Pst.string_of_pst p))    
    | Pst.Node _, _ -> raise (AbstractSyntaxError("Expected binding to start with a symbol but got " ^ Pst.string_of_pst p))
    | _ -> raise (AbstractSyntaxError ("Unexpected form: " ^ Pst.string_of_pst p))

let binding_of_string s =
  s
  |> Pstparser.pst_of_string
  |> binding_of_pst

let bindings_of_string s =
  let p = Pstparser.pstparser_of_string s in
  let rec parse_binding_list () =
    match Pstparser.parse_pst p with
    | None -> []
    | Some pst ->
       binding_of_pst pst :: parse_binding_list ()
  in
  parse_binding_list ()

let pattern_of_string s =
  s
  |> Pstparser.pst_of_string
  |> pattern_of_pst
